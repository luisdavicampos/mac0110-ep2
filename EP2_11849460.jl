# Parte 1

function checkWill(cards)
    v = zeros(4)
    for el in cards
        if el == 6
            v[1] = 1
        end
        if el == 1
            v[2] = 1
        end
        if el == 7
            v[3] = 1
        end
        if el == 4
            v[4] = 1
        end
    end

    soma = 0
    for i in 1:4
        soma += v[i]
    end
    if soma == 4
        return true
    else
        return false
    end
end

# Parte 2

function checkTaki(cards)
    v = zeros(4)
    for i in 1:length(cards)
        if cards[i] == 6
            v[1] = 1
        elseif cards[i] == 7 && v[3] < 3
            v[3] += 1
        elseif cards[i] == 1 && v[2] < 3
            v[2] += 1
        elseif cards[i] == 4
            v[4] = 1
        end
    end

    soma = 0
    for i in 1:4
        soma += v[i]
    end
    if soma == 8
        return true
    else
        return false
    end
end

# Parte 3

function checkJackson(x, cards)
    jack = 1
    n = x ÷ 10
    while n >= 1
        jack += 1
        n = n ÷ 10
    end

    v = zeros(4+jack)
    for i in 1:length(cards)
        if cards[i] == 6 && v[1] < 1
            v[1] = 1
            cards[i] = -1
        end
        if cards[i] == 1 && v[2] < 3
            v[2] += 1
            cards[i] = -1
        end
        if cards[i] == 7 && v[3] < 3
            v[3] += 1
            cards[i] = -1
        end
        if cards[i] == 4 && v[4] < 1
            v[4] = 1
            cards[i] = -1
        end
    end
    for i in 1:jack
        for j in 1:length(cards)
            if cards[j] == (x ÷ 10^(i-1)) % 10 && v[4+i] < 1
                v[4+i] = 1
                cards[j] = -1
            end
        end
    end

    soma = 0
    for i in 1:(4+jack)
        soma += v[i]
    end
    if jack == 0
        if soma == 9
            return true
        else
            return false
        end
    end
    if soma == 8 + jack
        return true
    else
        return false
    end
end

# Parte 4

function trocabase(x, b)
    soma = 0
    pot = 0
    while x != 0
        soma = soma + (x % b) * 10^pot
        pot += 1
        x = x ÷ b
    end
    return soma
end

function checkWillBase(b, cards)
    n = trocabase(6174, b)
    tam = 0
    x = n
    while x > 0
        tam += 1
        x = x ÷ 10
    end
    
    v = zeros(tam)
    for j in 1:tam
        for i in 1:length(cards)
            if cards[i] == (n ÷ 10^(j-1)) % 10 && v[j] < 1
                v[j] = 1
                cards[i] = -1
            end
        end
    end

    soma = 0
    for i in 1:tam
        soma += v[i]
    end
    if soma == tam
        return true
    else
        return false
    end
end